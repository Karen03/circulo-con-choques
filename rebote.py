from re import M
import pygame
import random

pygame.init()

swidth=500
sheight=500

screen= pygame.display.set_mode((swidth,sheight))
myclock=pygame.time.Clock()
ball_color=(255,0,0)
bg_color=(255,255,255)
ball_size=30
x=swidth/2
y=sheight/2
movimiento_x="izquierda"
movimiento_y="arriba"

sound1=pygame.mixer.Sound("Sonido.wav")
while True:
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            pygame.quit()
            
    
    screen.fill(bg_color)
    pygame.draw.circle(screen,ball_color,(x,y),ball_size)
    
    if (x+ball_size)>=500:
        movimiento_x="izquierda"
        sound1.play()
        
    if (x-ball_size)<=0:
        movimiento_x="derecha"
        sound1.play()
        
    if movimiento_x=="derecha":
        x+=1
    else:
        x-=1
        
        
    if (y+ball_size)>=500:
        movimiento_y="Abajo"
        sound1.play()
        
    if (y-ball_size)<=0:
        movimiento_y="Arriba"
        sound1.play()
        
    if movimiento_y=="Arriba":
        y+=random.randint(0,7)
    else:
        y-=random.randint(0,10)    
        
    pygame.display.update()
    myclock.tick(100)